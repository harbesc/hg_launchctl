**Keeps an hg repository up to date using the launchctl scheduler.**

Instructions:

1. Clone the repository into your $HOME directory.
2. Run ./hg_launchctl/scripts/provision.sh 
3. Enter the path to the hg repo you want to keep updated when prompted ().
    - Do not use a ~ in the path. launchctl config does not seem to handle that properly.
4. Enter the hour and minute when prompted.

That is all! Your plist file should now be loaded and set to run on the schedule you chose.
