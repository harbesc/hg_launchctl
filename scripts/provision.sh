#!/bin/bash

set -e

echo -n "Enter hg repo path (i.e. /Users/harbesc/foo): "
read -r
repo_path=$REPLY

cat <<EOF > $HOME/hg_launchctl/scripts/hg_fetch.sh
#!/bin/bash

set -e # Exit early if command fails

cd $repo_path && hg pull && hg up master
EOF

chmod +x $HOME/hg_launchctl/scripts/hg_fetch.sh

echo -n "Enter the hour you would like the job to run at (i.e. 3): "
read -r
hour=$REPLY

echo -n "Enter the minute you would like the job to run at (i.e. 30): "
read -r
minute=$REPLY

cat <<EOF > ~/Library/LaunchAgents/com.gitlab.harbesc.hg.plist
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.gitlab.harbesc.hg</string>
    <key>ProgramArguments</key>
    <array>
        <string>$HOME/hg_launchctl/scripts/hg_fetch.sh</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>StartCalendarInterval</key>
    <dict>
        <key>Minute</key>
        <integer>$minute</integer>
        <key>Hour</key>
        <integer>$hour</integer>
    </dict>
    <key>EnvironmentVariables</key>
    <dict>
        <key>PATH</key>
        <string><![CDATA[$PATH]]></string>
    </dict>
    <key>StandardOutPath</key>
    <string>$HOME/hg_launchctl/log/debug.log</string>
    <key>StandardErrorPath</key>
    <string>$HOME/hg_launchctl/log/debug.log</string>
</dict>
</plist>
EOF

set +e
launchctl unload ~/Library/LaunchAgents/com.gitlab.harbesc.hg.plist
touch $HOME/hg_launchctl/log/debug.log
set -e
launchctl load ~/Library/LaunchAgents/com.gitlab.harbesc.hg.plist

echo "Success! Your job has been scheduled for $hour:$minute. You can view the plist file at: ~/Library/LaunchAgents/com.gitlab.harbesc.hg.plist."
